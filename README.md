# Project 1 - Expense Reimbursement System (ERS) - JWA version 3.1
## Project Description
The Expense Reimbursement System (ERS) will manage the process of reimbursing employees for expenses incurred while on company time. All employees in the company can login and submit requests for reimbursement and view their pass tickets and pending requests. Finance managers can log in and view all reimbursement requests and past history for all employees in the company. Finance managers are authorized to approve and deny requests for expense reimbursement.

## Technologies Used
* Java, Javalin, JDBC, Oracle SQL
* HTML, CSS, JavaScript, Bootstrap, AJAX
* Mockito, JUnit

## Features
List of features ready and TODOs for future development
- [x] Login in/out functionality.
- [x] Encrypted password saved through the MariaDB
- [x] Once logged out, user will not able to view the profile.

To-do list:
- [ ] Improve the user interface for updating the table
- [ ] Clean the code up for the readability

## Getting Started
   - `git clone` [ERS](https://gitlab.com/cerberlink/project-1-ers.git)
   -  Open the project using Eclipse
   -  Execute the ers.sql in the MariaDB environment through DBeaver
   -  In `DBConnection`, there is an information such as URL, password, username.
   -  Run the application via Eclipse.

## Usage
- Once this appication runs successfully, please see the url: http://localhost:9001/ 
- It will take you to the login page, where you will able to view the username and password. If you do not have one, you can create your own username and password in the registration. (If you can use any type of browser such as chrome, safari, or firefore, go ahead).
- You can able to view the profile, new request, and change name if you wish.
- When you finish to create the request, you can log out. 
- log in as a manager, you will able to view the profile, pending, resolved, authorized to approve and deny the employee's request.
