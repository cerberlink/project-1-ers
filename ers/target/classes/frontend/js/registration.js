/**
 * 
 */
// The link to try and login at
const URL = window.location.href; // localhost:7001/:Manager?Employee


const SUCCESS = "registered";
const FAIL = "matched";


function onRegisterClick() {
	console.log("Button Clicked");
	
	let username = document.getElementById("username").value;
	let password = document.getElementById("password").value;
	let first_name = document.getElementById("fname").value;
	let last_name = document.getElementById("lname").value;
	let email = document.getElementById("email").value;
	var designations = document.getElementsByName('designation');
	var role_id;
	for (var i = 0; i < designations.length; i++) {
		if (designations[i].checked) {
			role_id = designations[i].value;
		}
	}
	
	console.log(username + ", " + password +", "+ role_id);
	
	let postData = "username=" + username +"&password=" + password + "&role_id=" + role_id + 
	"&first_name=" + first_name + "&last_name=" + last_name + "&email=" + email;
	
	sendAjaxPost(URL, postData, function (xhr) { // xhr = XMLHttpRequest
		console.log("Reponse.get:" + xhr.responseText);
		
		// If the response was a successful login, reload the correct page
		if (xhr.status === 200) {
			window.location.assign(xhr.responseText);
		}
		// Otherwise, inform the user of invalid login
		else {
			document.getElementById("invalidHeader").removeAttribute("hidden");
			console.log("Didn't Work");
		}
			
	});
}

function sendAjaxPost(url, postData, callback) {
	// Get a new XHR object, or an activeX object if the browser doesn't support XHL
	let xhr = (new XMLHttpRequest() || new ActiveXObject("Microsoft.HTTPRequest"));
	
	
	
	// Set the behavior when the ready state changes.
	xhr.onreadystatechange = function () {
		// If the request is done, and it returned a successful code, handle the weather
		if (this.readyState === 4 && this.status === 200) {
			//console.log("Received XHR Response: " + xhr.responseText);
			// Call the handler, passing this xhr object in. 
			callback(this);
		}
	}
	
	// Open and send the request. 
	xhr.open("POST", url);
	// Set the content type so the server knows what to expect
	xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
	xhr.send(postData);
}


// Add an event listener for the button. 
 document.getElementById("registerBtn").addEventListener("click", onRegisterClick);