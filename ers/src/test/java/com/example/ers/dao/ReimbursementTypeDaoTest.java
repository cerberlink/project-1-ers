package com.example.ers.dao;

import java.util.List;

import com.example.ers.ServiceLocator;
import com.example.ers.model.ReimbursementType;

import junit.framework.TestCase;

public class ReimbursementTypeDaoTest extends TestCase {
	
private DBConnection connection;
	
	public void setUp() throws Exception {
		connection = new DBConnection();
        super.setUp();
    }

    public void tearDown() throws Exception {
        super.tearDown();
    }

    public void testInsert() throws Exception {
        new ReimbursementTypeDao(connection).insert(new ReimbursementType(434,"Test type"));
    }

    public void testQueryAll() throws Exception {
        List<ReimbursementType> reimbursementTypes = new ReimbursementTypeDao(connection).queryAll();
        for (ReimbursementType reimbursementType:reimbursementTypes) {
            System.out.println(reimbursementType);
        }
    }

    public void testQueryById() throws Exception {
        System.out.println(new ReimbursementTypeDao(connection).queryById(3));
    }

    public void testGetId() throws Exception {
        System.out.println(new ReimbursementTypeDao(connection).getId());
    }
	
}
