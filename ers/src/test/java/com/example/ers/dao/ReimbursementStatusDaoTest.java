package com.example.ers.dao;

import java.util.List;

import com.example.ers.ServiceLocator;
import com.example.ers.model.ReimbursementStatus;

import junit.framework.TestCase;

public class ReimbursementStatusDaoTest extends TestCase {
	
private DBConnection connection;
	
	public void setUp() throws Exception {
		connection = new DBConnection();
        super.setUp();
    }

    public void tearDown() throws Exception {
        super.tearDown();
    }

    public void testInsert() throws Exception {
        ReimbursementStatus status = new ReimbursementStatus(23,"Test");
        new ReimbursementStatusDao(connection).insert(status);
    }

    public void testQueryAll() throws Exception {
        List<ReimbursementStatus> reimbursementStatuses = new ReimbursementStatusDao(connection).queryAll();
        for (ReimbursementStatus reimbursementStatus:reimbursementStatuses) {
            System.out.println(reimbursementStatus);
        }
    }

    public void testQueryById() throws Exception {
        System.out.println(new ReimbursementStatusDao(connection).queryById(3));
    }

    public void testGetId() throws Exception {
        System.out.println(new ReimbursementStatusDao(connection).getId());
    }
}
