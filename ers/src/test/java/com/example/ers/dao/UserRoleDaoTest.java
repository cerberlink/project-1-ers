package com.example.ers.dao;

import java.sql.Connection;
import java.util.List;

import com.example.ers.ServiceLocator;
import com.example.ers.model.UserRole;

import junit.framework.TestCase;

public class UserRoleDaoTest extends TestCase {
	
	private DBConnection connection;
	
	public void setUp() throws Exception {
		connection = new DBConnection();
        super.setUp();
    }

    public void tearDown() throws Exception {
        super.tearDown();
    }

    public void testInsert() throws Exception {
        new UserRoleDao(connection).insert(new UserRole(434,"Test role"));
    }

    public void testQueryAll() throws Exception {
        List<UserRole> userRoles = new UserRoleDao(connection).queryAll();
        for (UserRole userRole:userRoles) {
            System.out.println(userRole);
        }
    }

    public void testQueryById() throws Exception {
        System.out.println(new UserRoleDao(connection).queryById(6));
    }

    public void testGetId() throws Exception {
        System.out.println(new UserRoleDao(connection).getId());
    }
}
