package com.example.ers.dao;

import java.sql.Timestamp;
import java.util.List;

import com.example.ers.ServiceLocator;
import com.example.ers.model.Reimbursement;
import com.example.ers.model.ReimbursementStatus;
import com.example.ers.model.ReimbursementType;
import com.example.ers.model.User;

import junit.framework.TestCase;

public class ReimbursementDaoTest extends TestCase {
	
	DBConnection connection;

	public void setUp() throws Exception {
		connection = new DBConnection();
        super.setUp();
    }

    public void tearDown() throws Exception {
        super.tearDown();
    }

    public void testInsert() throws Exception {
        User author = new UserDao( connection).queryById(4);
        User resolver = new UserDao(connection).queryById(7);
        ReimbursementStatus status = new ReimbursementStatus(2, "Pending");
        ReimbursementType type = new ReimbursementType(3, "Food");
        Reimbursement reimbursement = new Reimbursement(213, 4352.4, Timestamp.valueOf("2016-03-01 13:03:24"), null, "This is a description", null, author, null, status, type);
        new ReimbursementDao(connection).insert(reimbursement);
    }

    public void testQueryAll() throws Exception {
        List<Reimbursement> reimbursements = new ReimbursementDao(connection).queryAll();
        for (Reimbursement reimbursement : reimbursements) {
            System.out.println(reimbursement);
        }
    }

    public void testApprove() throws Exception {
        Reimbursement reimbursement = new ReimbursementDao(connection).queryById(9);
        new ReimbursementDao(connection).approve(reimbursement, true);
    }

    public void testQueryByUser() throws Exception {
        User user = new User();
        user.setId(5);
        System.out.println(new ReimbursementDao(connection).queryByUser(user, 2));//Resolver
        System.out.println(new ReimbursementDao(connection).queryByUser(user, 1));//Author
    }

    public void testQueryPending() throws Exception {
        System.out.println(new ReimbursementDao(connection).queryPending());
    }

    public void testGetId() throws Exception {
        System.out.println(new ReimbursementDao(connection).getId());
    }

    public void testQueryById() throws Exception {
        System.out.println(new ReimbursementDao(connection).queryById(3));
        System.out.println(new ReimbursementDao(connection).queryById(4));
    }

}
