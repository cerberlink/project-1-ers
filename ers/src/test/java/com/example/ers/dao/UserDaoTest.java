package com.example.ers.dao;

import java.util.List;

import com.example.ers.ServiceLocator;
import com.example.ers.model.User;
import com.example.ers.model.UserRole;

import junit.framework.TestCase;

public class UserDaoTest extends TestCase {
	
	DBConnection connection;
	
	public void setUp() throws Exception {
		connection = new DBConnection();
        super.setUp();
    }

    public void tearDown() throws Exception {
        super.tearDown();
    }

    public void testInsert() throws Exception {
        UserRole role = new UserRole(4,"TestRole");
        User user = new User(23,"mnick","skdjfsdf","Mario","Nick","mnick@testm.com",role);
        new UserDao(connection).insert(user);
    }

    public void testQueryAll() throws Exception {
        List<User> users = new UserDao(connection).queryAll();
        for (User user:users) {
            System.out.println(user);
        }
    }

    public void testQueryByUsernameOrEmail() throws Exception {
        System.out.println(new UserDao(connection).queryByUsername("bclarkm"));
        System.out.println(new UserDao(connection).queryByEmail("mnick@testm.com"));
    }

    public void testQueryById() throws Exception {
        System.out.println(new UserDao(connection).queryById(6));
        System.out.println(new UserDao(connection).queryById(15));
    }

    public void testGetId() throws Exception {
        System.out.println(new UserDao(connection).getId());
    }
}
