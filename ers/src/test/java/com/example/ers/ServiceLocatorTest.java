package com.example.ers;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;

import com.example.ers.dao.DBConnection;

import junit.framework.TestCase;

public class ServiceLocatorTest extends TestCase {
	
	private DBConnection dbc;
	private Connection con;
	
	public void setUp() throws Exception {
		dbc = new DBConnection();
		con = dbc.getDBConnection();
        super.setUp();
    }

    public void tearDown() throws Exception {
    	con.close();
        super.tearDown();
    }
    
    public void testGetERSDatabase() throws Exception {
        Statement statement = ((Connection) con).createStatement();
        ResultSet resultSet = (statement).executeQuery("SELECT * FROM ers_users");
        while (resultSet.next()){
            System.out.println(resultSet.getString("user_email"));
        }
    }
}
