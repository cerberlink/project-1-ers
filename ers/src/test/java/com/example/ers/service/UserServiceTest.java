package com.example.ers.service;

import com.example.ers.exceptions.ExistingUserException;
import com.example.ers.model.User;
import com.example.ers.model.UserRole;

import junit.framework.TestCase;

public class UserServiceTest extends TestCase {
	
	public void setUp() throws Exception {
        super.setUp();
    }

    public void tearDown() throws Exception {

    }
    
//    public void testInsertNewUser() throws ExistingUserException {
//
//        new UserService().insertNewUser("nick","kobe","Ivan",
//                "Nick","nick@test.com",4);
//    }

    public void testCheckExistingUser() throws Exception {
        UserRole role = new UserRole(10, "Civil Engineer");
        User user = new User(1, "shamilton0","LdTPHY","Steven",
                "Hamilton","shamilton0@list-manage.com",role);
        System.out.println(new UserService().userExists("shamilton0","shamilton0@list-manage.com"));
    }

    public void testTestUserLogin() throws Exception {
        System.out.println(new UserService().userLogin("nick","kobe"));
    }
}
