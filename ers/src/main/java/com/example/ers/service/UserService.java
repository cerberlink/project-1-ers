package com.example.ers.service;

import java.util.List;

import com.example.ers.Authentication;
import com.example.ers.dao.DataFacade;
import com.example.ers.exceptions.ExistingUserException;
import com.example.ers.model.Reimbursement;
import com.example.ers.model.User;
import com.example.ers.model.UserRole;

public class UserService {
	
	void insertNewUser(String username, String password, String first_name, String last_name, String email, int role_id) throws ExistingUserException {
		
		if (userExists(username, email))
			throw new ExistingUserException("The user with username " + username + " and email " + email + " is already registered!");
		
		UserRole userRole = new DataFacade().getRole(role_id);
		
		password = Authentication.hash(password);
		
		User user = new User(username, password, first_name, last_name, email, userRole); // to remove 1, username, password, first_name, last_name, email, userRole into the parameter
		
		new DataFacade().insertUser(user);
	}
	
	public User userLogin(String username, String password) throws NullPointerException {
		User user = new DataFacade().getUserByUsername(username);
		if (Authentication.validatePassword(password, user.getPassword())) {
			return user;
		}
		return null;
	}
	
	boolean userExists(String username, String email) {
		
		return new DataFacade().getUserByEmail(email) != null && new DataFacade().getUserByUsername(username) != null;
	}
	
	public List<Reimbursement> getApprovedReimbursements(User user) {
		
		return new DataFacade().getAllApprovedReimbursements(user);
	}
	
	public List<Reimbursement> getDeclinedReimbursements(User user) {
		
		return new DataFacade().getAllDeclinedReimbursements(user);
	}
}
