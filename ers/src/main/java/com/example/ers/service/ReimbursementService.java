package com.example.ers.service;

import java.util.List;

import com.example.ers.dao.DataFacade;
import com.example.ers.model.Reimbursement;
import com.example.ers.model.User;

public class ReimbursementService {
	List<Reimbursement> getPending(){
        return new DataFacade().getAllPendingReimbursements();
    }

    public List<Reimbursement> updateBulk(List<Reimbursement> updated) {
        return new DataFacade().updateBulk(updated);
    }

    public List<Reimbursement> insertReimbursement(Reimbursement reimbursement) {
        return new DataFacade().insertReimbursement(reimbursement);
    }

    public List<Reimbursement> getAllReimbursements(User user) {
        return new DataFacade().getAllReimbursementsFromAuthor(user);
    }
    
    public void approveReimbursement(int remId) {
    	Reimbursement rem = new DataFacade().getReimbursementById(remId);
         new DataFacade().approveReimbursement(rem);
    }
    
    public void declineReimbursement(int remId) {
    	Reimbursement rem = new DataFacade().getReimbursementById(remId);
         new DataFacade().declineReimbursement(rem);
    }
}
