package com.example.ers.service;

import java.util.List;

import com.example.ers.dao.DataFacade;
import com.example.ers.model.Reimbursement;
import com.example.ers.model.ReimbursementType;

public class ReimbursementTypeService {
	
	public List<Reimbursement> getReimbursementTypes(){
		return new DataFacade().getAllReimbursementTypes();
	}
}
