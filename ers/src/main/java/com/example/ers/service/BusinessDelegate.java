package com.example.ers.service;

import java.util.List;

import com.example.ers.exceptions.ExistingUserException;
import com.example.ers.model.Reimbursement;
import com.example.ers.model.User;
import com.example.ers.model.UserRole;

/*
 * This file determines to implement each method that calls from Reimbursement, User, and Reimbursement_Type
 * This file helps to improve the reduction of complexity and manageability.
 */

public class BusinessDelegate {
	
	public User login(String username, String password) throws NullPointerException {
        return new UserService().userLogin(username, password);
    }

    public void register(String username, String password, String first_name, String last_name, String email, int role_id) throws ExistingUserException {
        new UserService().insertNewUser(username, password, first_name, last_name, email, role_id);
    }
    
    public List<Reimbursement> pendingReimbursements(){
        return new ReimbursementService().getPending();
    }

    public List<UserRole> getRoles() {
       return new UserRoleService().getRoles();
    }

    public List<Reimbursement> approvedReimbursements(User user) {
        return new UserService().getApprovedReimbursements(user);
    }

    public List<Reimbursement> declinedReimbursements(User user) {
        return new UserService().getApprovedReimbursements(user);
    }

    public List<Reimbursement> updateReimbursements(List<Reimbursement> updated) {
        return new ReimbursementService().updateBulk(updated);
    }

    public List<Reimbursement> getTypes() {
        return new ReimbursementTypeService().getReimbursementTypes();
    }

    public List<Reimbursement> insertReimbursement(Reimbursement reimbursement) {
        return new ReimbursementService().insertReimbursement(reimbursement);
    }

    public List<Reimbursement> allReimbursements(User user) {
        return new ReimbursementService().getAllReimbursements(user);
    }
    
    public void approveReimbursement(int remId) {
		 new ReimbursementService().approveReimbursement(remId);
	}
   
   public void declineReimbursement(int remId) {
		 new ReimbursementService().declineReimbursement(remId);
	}
}
