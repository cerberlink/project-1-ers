package com.example.ers.service;

import java.util.List;

import com.example.ers.dao.DataFacade;
import com.example.ers.model.UserRole;

public class UserRoleService {
	
	public List<UserRole> getRoles(){
		return new DataFacade().getAllRoles();
	}
}
