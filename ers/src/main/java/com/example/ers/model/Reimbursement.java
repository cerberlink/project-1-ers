package com.example.ers.model;


import java.sql.Timestamp;

import com.example.ers.ResourceHelper;

public class Reimbursement implements ResourceHelper {
	
	private int id;
    private double reimbAmount;
    private Timestamp reimbSubmitted;
    private Timestamp reimbResolved;
    private String reimbDescription;
    private String reimbReceipt;
    private User reimbAuthor;
    private User reimbResolver;
    private ReimbursementStatus reimbStatus;
    private ReimbursementType reimbType;

    public Reimbursement(int id, double reimbAmount, Timestamp reimbSubmitted, Timestamp reimbResolved, String reimbDescription, String getReceipt, User author, User user, ReimbursementStatus reimbStatus, ReimbursementType reimbType) {
        // above the parameters, added the cast it from user
    	this.id = id;
        this.reimbAmount = reimbAmount;
        this.reimbSubmitted = reimbSubmitted;
        this.reimbResolved = reimbResolved;
        this.reimbDescription = reimbDescription;
        this.reimbReceipt = getReceipt;
        this.reimbAuthor = (User) author; // add cast from User [fixed it]
        this.reimbResolver = (User) user;
        this.reimbStatus = reimbStatus;
        this.reimbType = reimbType;
    }
    
    public Reimbursement(double reimbAmount, Timestamp reimbSubmitted, Timestamp reimbResolved, String reimbDescription, String reimbReceipt, User author, User reimbResolver, ReimbursementStatus reimbStatus, ReimbursementType reimbType) {
        // above the parameters, added the cast it from user
        this.reimbAmount = reimbAmount;
        this.reimbSubmitted = reimbSubmitted;
        this.reimbResolved = reimbResolved;
        this.reimbDescription = reimbDescription;
        this.reimbReceipt = reimbReceipt;
        this.reimbAuthor = author; // add cast from User [fixed it]
        this.reimbResolver = reimbResolver;
        this.reimbStatus = reimbStatus;
        this.reimbType = reimbType;
    }
    
    public Reimbursement(Timestamp reimbSubmitted, Timestamp reimbResolved, String reimbDescription, String reimbReceipt, User author, User reimbResolver, ReimbursementStatus reimbStatus, ReimbursementType reimbType) {
        // above the parameters, added the cast it from user
        this.reimbSubmitted = reimbSubmitted;
        this.reimbResolved = reimbResolved;
        this.reimbDescription = reimbDescription;
        this.reimbReceipt = reimbReceipt;
        this.reimbAuthor = (User) author; // add cast from User [fixed it]
        this.reimbResolver = reimbResolver;
        this.reimbStatus = reimbStatus;
        this.reimbType = reimbType;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public double getReimbAmount() {
        return reimbAmount;
    }

    public void setReimbAmount(double reimbAmount) {
        this.reimbAmount = reimbAmount;
    }

    public Timestamp getReimbSubmitted() {
        return reimbSubmitted;
    }

    public void setReimbSubmitted(Timestamp reimbSubmitted) {
        this.reimbSubmitted = reimbSubmitted;
    }

    public Timestamp getReimbResolved() {
        return reimbResolved;
    }

    public void setReimbResolved(Timestamp reimbResolved) {
        this.reimbResolved = reimbResolved;
    }

    public String getReimbDescription() {
        return reimbDescription;
    }

    public void setReimbDescription(String reimbDescription) { this.reimbDescription = reimbDescription; }

    public String getReimbReceipt() {
        return reimbReceipt;
    }

    public void setReimbReceipt(String reimbReceipt) {
        this.reimbReceipt = reimbReceipt;
    }

    public User getReimbAuthor() {
        return reimbAuthor;
    }

    public void setReimbAuthor(User reimbAuthor) {
        this.reimbAuthor = reimbAuthor;
    }

    public User getReimbResolver() {
        return reimbResolver;
    }

    public void setReimbResolver(User reimbResolver) {
        this.reimbResolver = reimbResolver;
    }

    public ReimbursementStatus getReimbStatus() {
        return reimbStatus;
    }

    public void setReimbStatus(ReimbursementStatus reimbStatus) {
        this.reimbStatus = reimbStatus;
    }

    public ReimbursementType getReimbType() {
        return reimbType;
    }

    public void setReimbType(ReimbursementType reimbType) {
        this.reimbType = reimbType;
    }

    @Override
    public String toString() {
        return "Reimbursement{" +
                "id=" + id +
                ",\n\treimbAmount=" + reimbAmount +
                ",\n\treimbSubmitted=" + reimbSubmitted +
                ",\n\treimbResolved=" + reimbResolved +
                ",\n\treimbDescription='" + reimbDescription + '\'' +
                ",\n\treimbReceipt=" + reimbReceipt +
                ",\n\treimbAuthor=" + reimbAuthor +
                ",\n\treimbResolver=" + reimbResolver +
                ",\n\treimbStatus=" + reimbStatus +
                ",\n\treimbType=" + reimbType +
                "\n\t}";
    }
}
