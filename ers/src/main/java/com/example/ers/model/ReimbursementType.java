package com.example.ers.model;

import com.example.ers.ResourceHelper;

public class ReimbursementType implements ResourceHelper {
	
	private int id;
    private String type;

    public ReimbursementType() {
    	
    }

    public ReimbursementType(int id, String type) {
        this.id = id;
        this.type = type;
    }
    
    public ReimbursementType(String type) {
        this.type = type;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
    	this.type = type;
    }

    @Override
    public String toString() {
        return "ReimbursementType{" +
                "id=" + id +
                ", type='" + type + '\'' +
                '}';
    }
}
