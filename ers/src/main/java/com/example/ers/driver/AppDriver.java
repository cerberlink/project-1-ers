package com.example.ers.driver;

import static io.javalin.apibuilder.ApiBuilder.get;
import static io.javalin.apibuilder.ApiBuilder.path;
import static io.javalin.apibuilder.ApiBuilder.post;

import com.example.ers.auth.SessionAuth;
import com.example.ers.controller.FrontController;

import io.javalin.Javalin;

public class AppDriver {
	public static void main(String[] args) {

		Javalin app = Javalin.create(config -> {
			config.addStaticFiles("/frontend");
			config.sessionHandler(SessionAuth::fileSessionHandler);
		});

		// default port
		app.start(9001);

		app.routes(() -> {
			path("/", () -> {
				post("ers/new_reimbursement", ctx -> new FrontController().doPost.handle(ctx));
				get(ctx -> new FrontController().doGet.handle(ctx));
				post(ctx -> new FrontController().doPost.handle(ctx));
				path("register", () -> {
					get(ctx -> new FrontController().doGet.handle(ctx));
					post(ctx -> new FrontController().doPost.handle(ctx));
				});
				path("employee", () -> {
					get(ctx -> new FrontController().doGet.handle(ctx));
					get("/pending", ctx -> ctx.redirect("/ers/pending"));
					get("/approved", ctx -> ctx.redirect("/ers/approved"));
					get("/profile", ctx -> ctx.redirect("/ers/profile"));
				});
				path("manager", () -> {
					get(ctx ->new FrontController().doGet.handle(ctx));
					get("/pending", ctx -> ctx.redirect("/ers/pending"));
					get("/allPending", ctx -> ctx.redirect("/ers/allPending"));
					get("/approved", ctx -> ctx.redirect("/ers/approved"));
					get("/allResolved", ctx -> ctx.redirect("/ers/allResolved"));
					get("/allEmployees", ctx -> ctx.redirect("/ers/allEmployees"));
					get("/profile", ctx -> ctx.redirect("/ers/profile"));
					get("/approveDeny", ctx -> new FrontController().doGet.handle(ctx));
				});
				path("ers", () -> {
					get("/pending", ctx -> new FrontController().doGet.handle(ctx));
					get("/allPending", ctx -> new FrontController().doGet.handle(ctx));
					get("/approved", ctx -> new FrontController().doGet.handle(ctx));
					get("/allResolved", ctx -> new FrontController().doGet.handle(ctx));
					get("/profile", ctx -> new FrontController().doGet.handle(ctx));
					get("/allEmployees", ctx -> new FrontController().doGet.handle(ctx));

				});

				path("logout", () -> {
					get(ctx -> {
						new FrontController().doGet.handle(ctx);
					});
				});
			});
		});

//		app.exception(NullPointerException.class, (e, ctx)->{
//			//e.printStackTrace();
//			ctx.status(404);
//			ctx.result("error endpoint 404");
//		});
	}
}
