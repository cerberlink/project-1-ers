package com.example.ers.util;

import org.apache.log4j.Logger;

public class logUtil {
	private static Logger log = Logger.getRootLogger();
	
	// Prevents instantiation
	private logUtil() {}
	
	public static void logInfo(Object obj) {
		log.info(obj);
	}
	
	
	public static void logDebug(Object obj) {
		StackTraceElement ste = Thread.currentThread().getStackTrace()[2];
		StringBuilder sb = new StringBuilder();
		sb.append(ste.getClassName());
		sb.append(", line ");
		sb.append(ste.getLineNumber());
		sb.append(": ");
		
		// Account for null objects
		if (obj == null) {
			sb.append(""+null);
		} else {
			sb.append(obj.toString());
		}
		
		log.debug(sb);
	}
}
