package com.example.ers.util;

public class Path {

	public static class Web {
        public static final String INDEX = "/";
        public static final String LOGIN = "/login";
        public static final String REGISTARTION = "/register";
        public static final String LOGOUT = "/logout";
        public static final String EMPLOYEE = "employee";
        public static final String MANAGER = "manager";
    }

    public static class Template {
    	public static final String INDEX = "/frontend/html/login.html";
        public static final String LOGIN = "/frontend/html/login.html";
        public static final String REGISTARTION = "/frontend/html/Registration.html";
        public static final String MANAGER = "/frontend/html/Manager.html";
        public static final String EMPLOYEE = "/frontend/html/Employee.html";
//        public static final String NOT_FOUND = "/frontend/html/login.html";
    }
}
