package com.example.ers.exceptions;

@SuppressWarnings("serial")
public class LoginFailedException extends NullPointerException {
	
	public LoginFailedException() {
		
	}
	
	public LoginFailedException(String message) {
		super(message);
	}
}
