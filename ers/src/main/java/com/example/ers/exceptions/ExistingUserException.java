package com.example.ers.exceptions;

@SuppressWarnings("serial")
public class ExistingUserException extends Exception {
	
	public ExistingUserException(String message) {
		super(message);
	}
}
