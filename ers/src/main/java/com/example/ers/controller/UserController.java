package com.example.ers.controller;

import java.util.List;

import com.example.ers.exceptions.ExistingUserException;
import com.example.ers.model.Reimbursement;
import com.example.ers.model.User;
import com.example.ers.service.BusinessDelegate;
import com.example.ers.util.Path;

import io.javalin.http.Handler;

public class UserController {
	
	public Handler login = (ctx) -> {
		// DEBUG
		try {
			String username = ctx.formParam("username");
			String password = ctx.formParam("password");
            User user = new BusinessDelegate().login(username, password);
            user.setPassword(null);
            ctx.sessionAttribute("userData",user);
            if(user.getRole().getId()==1){
            	System.out.println("If "+ctx.body());
                List<Reimbursement> pending = new BusinessDelegate().pendingReimbursements();
                ctx.attribute("reimbursements",pending);
                ctx.attribute("selectedTab", "pending");
                ctx.html(Path.Web.MANAGER);
            }
            else{
            	System.out.println("Else "+ctx.body());
                List<Reimbursement> types = new BusinessDelegate().getTypes();
                List<Reimbursement> reimbursements = new BusinessDelegate().allReimbursements(user);
                ctx.attribute("reimbTypes",types);
                ctx.attribute("reimbursements",reimbursements);
                ctx.html(Path.Web.EMPLOYEE);
            }
        } catch (NullPointerException e){
        	System.out.println("in exception");
            ctx.attribute("message","Invalid username/password combination!");
            ctx.attribute("error",true);
            ctx.html("login.html");
        }
	};
	
	public Handler register = (ctx) -> {
		String username = ctx.formParam("username");
        String password = ctx.formParam("password");
        String first_name = ctx.formParam("first_name");
        String last_name = ctx.formParam("last_name");
        String email = ctx.formParam("email");
        int role_id = Integer.parseInt(ctx.formParam("role_id"));
        try {
            new BusinessDelegate().register(username,password,first_name,last_name,email,role_id);
            ctx.attribute("message","Successfully registered!");
            ctx.attribute("error",false);
            ctx.html("/");
        } catch (ExistingUserException e) {
            ctx.attribute("message",e.getMessage());
            ctx.attribute("error",false);
            ctx.html("/register");
        }
		
	};
	
	public Handler loggedInUser = (ctx) -> {
		User user = ctx.sessionAttribute("userData");
		ctx.json(user);
	};
}
