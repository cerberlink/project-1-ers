package com.example.ers.controller;


import io.javalin.http.Handler;
import com.example.ers.util.Path;

public class FrontController {

	public Handler doGet = (ctx) -> {
		System.out.println("url "+ ctx.fullUrl());
		switch(ctx.req.getRequestURI()) {
		case "/":
			if(ctx.sessionAttribute("userData") != null) new ReimbursementController().pending.handle(ctx);
			else ctx.render(Path.Template.LOGIN);
			break;
		case "/manager":
			 ctx.render(Path.Template.MANAGER);
			break;
		case "/employee":
			ctx.render(Path.Template.EMPLOYEE);
			break;
		case "/register":
            if(ctx.sessionAttribute("userData")!=null) new ReimbursementController().pending.handle(ctx);
            else ctx.render(Path.Template.REGISTARTION);
            break;
		case "/manager/approveDeny":
			 if(Boolean.parseBoolean(ctx.queryParams("bool").get(0))) {
	            	new ReimbursementController().approve.handle(ctx);
			        break;
	            }
	            else {
	            	new ReimbursementController().decline.handle(ctx);
		            break;
	            }
        case "/ers/pending":
            new ReimbursementController().pending.handle(ctx);
            break;
        case "/ers/allPending":
            new ReimbursementController().pending.handle(ctx);
            break;
        case "/ers/approved":
            new ReimbursementController().approved.handle(ctx);
            break;
        case "/ers/allResolved":
            new ReimbursementController().approved.handle(ctx);
            break;
        case "/ers/declined":
        	 ctx.html("Work in progress");
            break;
        case "/ers/profile":
        	 new UserController().loggedInUser.handle(ctx);
            break;
        case "/ers/allEmployees":
        	 ctx.html("Work in progress");
            break;
//        case "/ers/get_receipt":
//            new ReimbursementController().getReceipt.handle(ctx);
//            break;
        case "/logout":
        	 ctx.sessionAttribute("userData", null);
//            ctx.html(Path.Web.INDEX);
            break;
		}
	};
	
	public Handler doPost = (ctx)->{
		switch (ctx.req.getRequestURI()) {
        case "/":
            new UserController().login.handle(ctx);
            break;
        case "/register":
            new UserController().register.handle(ctx);
            break;
        case "/ers/updateReimbursements":
            new ReimbursementController().updateReimbursements.handle(ctx);
            break;
        case "/ers/new_reimbursement":
            new ReimbursementController().insertReimbursement.handle(ctx);
            break;
        default:
            ctx.status(404);
		}
	};
}
