package com.example.ers.controller;

import java.io.InputStream;
import java.io.OutputStream;
import java.sql.Blob;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.Part;

import com.example.ers.model.Reimbursement;
import com.example.ers.model.ReimbursementStatus;
import com.example.ers.model.ReimbursementType;
import com.example.ers.model.User;
import com.example.ers.service.BusinessDelegate;

import io.javalin.http.Handler;

public class ReimbursementController {

	public Handler pending = (ctx) -> {
		List<Reimbursement> reimbursements = new BusinessDelegate().pendingReimbursements();
		ctx.sessionAttribute("reimbursements", reimbursements);
		ctx.sessionAttribute("selectedTab", "pending");
		ctx.json(reimbursements);
	};

	public Handler approved = (ctx) -> {
		List<Reimbursement> reimbursements = new BusinessDelegate()
				.approvedReimbursements((User) ctx.sessionAttribute("userData"));
		ctx.sessionAttribute("reimbursements", reimbursements);
		ctx.sessionAttribute("selectedTab", "approved");
		ctx.json(reimbursements);
	};

	public Handler declined = (ctx) -> {
		List<Reimbursement> reimbursements = new BusinessDelegate()
				.declinedReimbursements((User) ctx.sessionAttribute("userData"));
		ctx.sessionAttribute("reimbursements", reimbursements);
		ctx.sessionAttribute("selectedTab", "declined");
		ctx.json(reimbursements);
	};

	public Handler approve = (ctx) -> {
		System.out.println("approval start");
		int remId = Integer.parseInt(ctx.queryParams("remId").get(0));
		new BusinessDelegate().approveReimbursement(remId);
		ctx.result("Approved");
		System.out.println("approval end");
	};

	public Handler decline = (ctx) -> {
		int remId = Integer.parseInt(ctx.queryParams("remId").get(0));
		new BusinessDelegate().declineReimbursement(remId);
		ctx.result("decline");
	};

	public Handler updateReimbursements = (ctx) -> {
		List<Reimbursement> reimbursementList = (List<Reimbursement>) ctx.sessionAttribute("reimbursements");
		List<Reimbursement> updatedReimbursements = new ArrayList<>();
		User user = (User) ctx.sessionAttribute("userData");
		Reimbursement temp;
		if (ctx.queryParam("approved") != null)
			for (Reimbursement r : reimbursementList) {
				String id = String.valueOf(r.getId());
				temp = reimbursementList.get(Integer.parseInt(id));
				temp.setReimbResolver(user);
				temp.getReimbStatus().setId(1);
				temp.getReimbStatus().setStatus("Approved");
				temp.setReimbResolved(new Timestamp(System.currentTimeMillis()));
				updatedReimbursements.add(temp);
			}
		System.out.println(ctx.queryParam("denied"));
		if (ctx.queryParam("denied") != null)
			for (Reimbursement r : reimbursementList) {
				String id = String.valueOf(r.getId());
				temp = reimbursementList.get(Integer.parseInt(id));
				temp.setReimbResolver(user);
				temp.getReimbStatus().setId(3);
				temp.getReimbStatus().setStatus("Denied");
				temp.setReimbResolved(new Timestamp(System.currentTimeMillis()));
				updatedReimbursements.add(temp);
			}
		ctx.sessionAttribute("reimbursements", new BusinessDelegate().updateReimbursements(updatedReimbursements));
		ctx.sessionAttribute("selectedTab", "pending");
		ctx.html("login.html");
	};

	// HOLD IT IN A WHILE.
//	public Handler getAllReceipt = (ctx) -> {
//		List<Reimbursement> reimbursementList = (List<Reimbursement>) ctx.sessionAttribute("reimbursements");
//        Reimbursement reimbursement = reimbursementList.get(Integer.parseInt(ctx.queryParam("reimbId")));
//        Blob inputStream = reimbursement.getReimbReceipt();
//        byte[] buffer = new byte[4096];
//        int bytesRead = -1;
//        OutputStream outStream = ctx.getOutputStream();
//        while ((bytesRead = inputStream.read(buffer)) != -1) {
//            outStream.write(buffer, 0, bytesRead);
//        }
//        outStream.flush();
//        outStream.close();
//        Blob.close();
//	};

	public Handler insertReimbursement = (ctx) -> {
		try {
			ReimbursementType reimbursementType = new ReimbursementType(Integer.parseInt(ctx.formParam("type")), null);
			ReimbursementStatus reimbursementStatus = new ReimbursementStatus(2, null);
			Double amount = Double.parseDouble(ctx.formParam("amount"));
			String description = ctx.formParam("description");
			String receipt = ctx.formParam("receipt");
			User author = (User) ctx.sessionAttribute("userData");
			// InputStream receipt = filePart.body();
			Reimbursement reimbursement = new Reimbursement(1, amount, new Timestamp(System.currentTimeMillis()), null,
					description, receipt, author, null, reimbursementStatus, reimbursementType);
			List<Reimbursement> reimbursementList = new BusinessDelegate().insertReimbursement(reimbursement);
			// ctx.sessionAttribute("reimbursements", reimbursementList);
			ctx.json(reimbursementList);
			// ctx.html("reimbursement added");
		} catch (IllegalStateException e) {
			ctx.sessionAttribute("message", "The file exceeded the 5MB limit!");
			ctx.html("Failed to add reimbursement");
		}
	};

}
