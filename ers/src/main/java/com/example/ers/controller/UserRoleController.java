package com.example.ers.controller;

import java.util.List;

import com.example.ers.model.UserRole;
import com.example.ers.service.BusinessDelegate;

public class UserRoleController {
	public List<UserRole> getRoles(){
        return new BusinessDelegate().getRoles();
    }

}
