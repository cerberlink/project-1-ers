package com.example.ers;

import org.mindrot.jbcrypt.BCrypt;
/*
 * Documentation - 
 * 	https://www.mindrot.org/projects/jBCrypt/
 * 	https://github.com/jeremyh/jBCrypt
 * 
 */
public class Authentication {
	
	public static String hash(String password) {
        return BCrypt.hashpw(password, BCrypt.gensalt(12));
    }

    public static boolean validatePassword(String candidate, String hashed) {
        return BCrypt.checkpw(candidate, hashed);
    }
}
