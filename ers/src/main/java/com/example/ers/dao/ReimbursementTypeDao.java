package com.example.ers.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.example.ers.model.ReimbursementType;

public class ReimbursementTypeDao implements Dao<ReimbursementType> {

	private DBConnection dbc;

    public ReimbursementTypeDao(DBConnection dbc) {
        this.dbc = dbc;
    }

    public void insert(ReimbursementType object) throws SQLException {
    	try(Connection con = dbc.getDBConnection()){
	        String sql = "INSERT INTO " + reimbursementTypeTableName + " VALUES (?,?)";
	        PreparedStatement preparedStatement = ((Connection) con).prepareStatement(sql);
	        preparedStatement.setInt(1, getId());
	        preparedStatement.setString(2, object.getType());
	        preparedStatement.executeUpdate();
    	}
    }

    public List<ReimbursementType> queryAll() throws SQLException {
    	try(Connection con = dbc.getDBConnection()){
	        List<ReimbursementType> typeList = new ArrayList<ReimbursementType>();
	        PreparedStatement preparedStatement = ((Connection) con).prepareStatement(getAllReimbursementTypes);
	        ResultSet resultSet = preparedStatement.executeQuery();
	        mapRows(resultSet,typeList);
	        return typeList;
    	}
    }

    public ReimbursementType queryById(int id) throws SQLException {
    	try(Connection con = dbc.getDBConnection()){
	        String sql = getAllReimbursementTypes + " WHERE " + reimbursementTypeId + "= ?";
	        PreparedStatement preparedStatement = ((Connection) con).prepareStatement(sql);
	        preparedStatement.setInt(1, id);
	        ResultSet resultSet = preparedStatement.executeQuery();
	        List<ReimbursementType> userList = new ArrayList<ReimbursementType>();
	        mapRows(resultSet, userList);
	        return userList.get(0);
    	}
    }

    public int getId() throws SQLException {
    	try(Connection con = dbc.getDBConnection()){
	        String sql = "SELECT MAX(" + reimbursementTypeId + ") FROM " + reimbursementTypeTableName;
	        PreparedStatement preparedStatement = ((Connection) con).prepareStatement(sql);
	        ResultSet resultSet = preparedStatement.executeQuery();
	        if (resultSet.next()) {
	             return resultSet.getInt(1) + 1;
	        } else throw new SQLException();
    	}
    }

    public void mapRows(ResultSet resultSet, List<ReimbursementType> list) throws SQLException {
    	try(Connection con = dbc.getDBConnection()){
	        while (resultSet.next()){
	            ReimbursementType type = new ReimbursementType(
	                resultSet.getInt(reimbursementTypeId),
	                resultSet.getString(reimbursementType)
	            );
	            list.add(type);
	        }
    	}
    }
}
