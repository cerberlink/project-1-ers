package com.example.ers.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.example.ers.model.UserRole;

public class UserRoleDao implements Dao<UserRole> {

	private DBConnection dbc;

    UserRoleDao(DBConnection dbc) {
        this.dbc = dbc;
    }

    public void insert(UserRole object){
    	try(Connection con = dbc.getDBConnection()){
	        String sql = "INSERT INTO " + userRoleTableName + " VALUES (?,?)";
	        PreparedStatement preparedStatement = con.prepareStatement(sql);
	        preparedStatement.setInt(1, getId());
	        preparedStatement.setString(2, object.getRole());
	        preparedStatement.executeUpdate();
    	} catch(SQLException e) {
    		e.printStackTrace();
    	}
    }

    public List<UserRole> queryAll() throws SQLException {
    	try(Connection con = dbc.getDBConnection()){
	        List<UserRole> typeList = new ArrayList<UserRole>();
	        PreparedStatement preparedStatement = con.prepareStatement(getAllUserRoles);
	        ResultSet resultSet = preparedStatement.executeQuery();
	        mapRows(resultSet, typeList);
	        return typeList;
    	}
    }

    public UserRole queryById(int id) throws SQLException {
    	try(Connection con = dbc.getDBConnection()){
	        List<UserRole> userRoleList = new ArrayList<UserRole>();
	        String sql = getAllUserRoles + " WHERE " + userRoleId + "= ?";
	        PreparedStatement preparedStatement = con.prepareStatement(sql);
	        preparedStatement.setInt(1, id);
	        ResultSet resultSet = preparedStatement.executeQuery();
	        mapRows(resultSet, userRoleList);
	        return userRoleList.get(0);
    	}
    }

    public int getId() throws SQLException {
    	try(Connection con = dbc.getDBConnection()){
	        String sql = "SELECT MAX(" + userRoleId + ") FROM " + userRoleTableName;
	        PreparedStatement preparedStatement = con.prepareStatement(sql);
	        ResultSet resultSet = preparedStatement.executeQuery();
	        if (resultSet.next()) {
	            return resultSet.getInt(1) + 1;
	        } else throw new SQLException();
    	}
    }

    public void mapRows(ResultSet resultSet, List<UserRole> list) throws SQLException {
    	try(Connection con = dbc.getDBConnection()){
	        while (resultSet.next()) {
	            UserRole role = new UserRole(
	                    resultSet.getInt(userRoleId),
	                    resultSet.getString(userRole)
	            );
	            list.add(role);
	        }
    	}
    }
}
