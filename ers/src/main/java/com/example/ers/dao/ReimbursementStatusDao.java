package com.example.ers.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.example.ers.model.ReimbursementStatus;

public class ReimbursementStatusDao implements Dao<ReimbursementStatus>{
	
	private DBConnection dbc;

    ReimbursementStatusDao(DBConnection dbc) {
        this.dbc = dbc;
    }

	@Override
	public void insert(ReimbursementStatus object) throws SQLException {
		// TODO Auto-generated method stub
		
	}

    public List<ReimbursementStatus> queryAll() throws SQLException {
    	try(Connection con = dbc.getDBConnection()){
	        List<ReimbursementStatus> reimbursementStatusList = new ArrayList<ReimbursementStatus>();
	        PreparedStatement preparedStatement = con.prepareStatement(getAllReimbursementStatuses);
	        ResultSet resultSet = preparedStatement.executeQuery();
	        mapRows(resultSet, reimbursementStatusList);
	        return reimbursementStatusList;
    	}
    }

    public ReimbursementStatus queryById(int id) throws SQLException {
    	try(Connection con = dbc.getDBConnection()){
	        String sql = getAllReimbursementStatuses + " WHERE " + reimbursementStatusId + "= ?";
	        PreparedStatement preparedStatement = con.prepareStatement(sql);
	        preparedStatement.setInt(1, id);
	        ResultSet resultSet = preparedStatement.executeQuery();
	        List<ReimbursementStatus> reimbursementList = new ArrayList<ReimbursementStatus>();
	        mapRows(resultSet, reimbursementList);
	        resultSet.close();
	        return reimbursementList.get(0);
    	}
    }

    public int getId() throws SQLException {
    	try(Connection con = dbc.getDBConnection()){
	        int id = 0;
	        String sql = "SELECT MAX(" + reimbursementStatusId + ") FROM " + reimbursementStatusTableName;
	        PreparedStatement preparedStatement = con.prepareStatement(sql);
	        ResultSet resultSet = preparedStatement.executeQuery();
	        if (resultSet.next()) id = resultSet.getInt(1) + 1;
	        return id;
    	}
    }

    public void mapRows(ResultSet resultSet, List<ReimbursementStatus> list) throws SQLException {
    	try(Connection con = dbc.getDBConnection()){
	        while (resultSet.next()) {
	            ReimbursementStatus reimbursementStatus = new ReimbursementStatus(
	                    resultSet.getInt(1),
	                    resultSet.getString(2)
	            );
	            list.add(reimbursementStatus);
	        }
    	}
    }
}
