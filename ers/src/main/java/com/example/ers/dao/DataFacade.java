package com.example.ers.dao;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

import com.example.ers.ServiceLocator;
import com.example.ers.model.Reimbursement;
import com.example.ers.model.ReimbursementType;
import com.example.ers.model.User;
import com.example.ers.model.UserRole;

public class DataFacade {
    private DBConnection connection = new DBConnection();

	public List<Reimbursement> insertReimbursement(Reimbursement reimbursement) {
        List<Reimbursement> reimbursementList = null;
        try {
            ((Connection) connection.getDBConnection()).setAutoCommit(false);
            System.out.println("innserting data");
            ReimbursementDao reimbursementDAO = new ReimbursementDao(connection);
            reimbursementDAO.insert(reimbursement);
            reimbursementList = reimbursementDAO.queryByUser(reimbursement.getReimbAuthor(),1);
            ((Connection) connection).commit();
        } catch (SQLException e) {
            e.printStackTrace();
            try {
                ((Connection) connection).rollback();
            } catch (SQLException e1) {
                e1.printStackTrace();
            }
        } finally {
            try {
                ((Connection) connection).close();
            } catch (SQLException e) {
                e.printStackTrace();
            } finally {
                return reimbursementList;
            }
        }
    }

    public List<Reimbursement> getAllReimbursements() {
        List<Reimbursement> list = null;
        try {
//            connection = ServiceLocator.getERSDatabase().getConnection();
            list = new ReimbursementDao(connection).queryAll();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                ((Connection) connection).close();
            } catch (SQLException e) {
                e.printStackTrace();
            } finally {
                return list;
            }
        }
    }

    public List<Reimbursement> getAllReimbursementsFromAuthor(User author) {
        List<Reimbursement> list = null;
        try {
//            connection = ServiceLocator.getERSDatabase().getConnection();
            list = new ReimbursementDao(connection).queryByUser(author, 1);
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                ((Connection) connection).close();
            } catch (SQLException e) {
                e.printStackTrace();
            } finally {
                return list;
            }
        }
    }

    public List<Reimbursement> getAllReimbursementsFromResolver(User resolver) {
        List<Reimbursement> list = null;
        try {
            //connection = ServiceLocator.getERSDatabase().getConnection();
            list = new ReimbursementDao(connection).queryByUser(resolver, 2);
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                ((Connection) connection).close();
            } catch (SQLException e) {
                e.printStackTrace();
            } finally {
                return list;
            }
        }
    }

    public Reimbursement getReimbursementById(int id) {
        Reimbursement reimbursement = null;
        try {
            //connection = ServiceLocator.getERSDatabase().getConnection();
            reimbursement = new ReimbursementDao(connection).queryById(id);
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                ((Connection) connection).close();
            } catch (SQLException e) {
                e.printStackTrace();
            } finally {
                return reimbursement;
            }
        }
    }

    public void approveReimbursement(Reimbursement reimbursement) {
        try {
            //connection = ServiceLocator.getERSDatabase().getConnection();
            new ReimbursementDao(connection).approve(reimbursement, true);
        } catch (SQLException e) {
            e.printStackTrace();
        } 
     }

    public void declineReimbursement(Reimbursement reimbursement) {
        try {
            //connection = ServiceLocator.getERSDatabase().getConnection();
            new ReimbursementDao(connection).approve(reimbursement, false);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public List<Reimbursement> getAllPendingReimbursements() {
        List<Reimbursement> list = null;
        try(Connection con = connection.getDBConnection()) {
            //connection = ServiceLocator.getERSDatabase().getConnection();
            list = new ReimbursementDao(connection).queryPending();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                ((Connection) connection).close();
            } catch (SQLException e) {
                e.printStackTrace();
            } finally {
                return list;
            }
        }
    }

    public void insertUser(User user) {
    	try(Connection con = connection.getDBConnection()) {
            //connection = ServiceLocator.getERSDatabase().getConnection();
            new UserDao(connection).insert(user);
        } catch (SQLException e) {
            e.printStackTrace();
       }
    }

    public List<User> getAllUsers() {
        List<User> list = null;
        try {
            //connection = ServiceLocator.getERSDatabase().getConnection();
            list = new UserDao(connection).queryAll();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                ((Connection) connection).close();
            } catch (SQLException e) {
                e.printStackTrace();
            } finally {
                return list;
            }
        }
    }

    public User getUserByUsername(String username) {
    	System.out.println("inside getUserByUsername");
        User user = null;
        try(Connection con = connection.getDBConnection()) {
        	System.out.println("inside try");
        	
            //connection = ServiceLocator.getERSDatabase().getConnection();
            user = new UserDao(connection).queryByUsername(username);
        } catch (SQLException e) {
        	System.out.println("innside catch");
        	e.printStackTrace();
        } finally {
        	System.out.println("inside finally");
            try {
                ((Connection) connection).close();
            } catch (SQLException e) {
                e.printStackTrace();
            } finally {
                return user;
            }
        }
    }

    public User getUserByEmail(String email) {
        User user = null;
        try {
            //connection = ServiceLocator.getERSDatabase().getConnection();
            user = new UserDao(connection).queryByEmail(email);
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                ((Connection) connection).close();
            } catch (SQLException e) {
                e.printStackTrace();
            } finally {
                return user;
            }
        }
    }

    public List<UserRole> getAllRoles() {
        List<UserRole> userRoles = null;
        try {
            //connection = ServiceLocator.getERSDatabase().getConnection();
            userRoles = new UserRoleDao(connection).queryAll();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                ((Connection) connection).close();
            } catch (SQLException e) {
                e.printStackTrace();
            } finally {
                return userRoles;
            }
        }
    }

    public UserRole getRole(int id) {
    	UserRole role = null;
        try {
            //connection = ServiceLocator.getERSDatabase().getConnection();
            role = new UserRoleDao(connection).queryById(id);
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                ((Connection) connection).close();
            } catch (SQLException e) {
                e.printStackTrace();
            } finally {
                return role;
            }
        }
    }


    public List<Reimbursement> getAllApprovedReimbursements(User user) {
        List<Reimbursement> reimbursements = null;
        try {
            //connection = ServiceLocator.getERSDatabase().getConnection();
            reimbursements = new ReimbursementDao(connection).queryAccepted(user);
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                ((Connection) connection).close();
            } catch (SQLException e) {
                e.printStackTrace();
            } finally {
                return reimbursements;
            }
        }

    }

    public List<Reimbursement> getAllDeclinedReimbursements(User user) {
        List<Reimbursement> reimbursements = null;
        try {
            //connection = ServiceLocator.getERSDatabase().getConnection();
            reimbursements = new ReimbursementDao(connection).queryDeclined(user);
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                ((Connection) connection).close();
            } catch (SQLException e) {
                e.printStackTrace();
            } finally {
                return reimbursements;
            }
        }
    }

    public List<Reimbursement> updateBulk(List<Reimbursement> updated) {
        //TODO finish this update bulk function
        List<Reimbursement> reimbursements = null;
        try {
            //connection = ServiceLocator.getERSDatabase().getConnection();
            ((Connection) connection).setAutoCommit(false);
            ReimbursementDao reimbursementDAO = new ReimbursementDao(connection);
            for (Reimbursement reimb:
                 updated) {
                if(reimb.getReimbStatus().getId()==1)
                    reimbursementDAO.approve(reimb,true);
                else if(reimb.getReimbStatus().getId()==3)
                    reimbursementDAO.approve(reimb,false);
            }
            ((Connection) connection).commit();
            reimbursements = reimbursementDAO.queryPending();
        } catch (SQLException e) {
            e.printStackTrace();
            try {
                ((Connection) connection).rollback();
            } catch (SQLException e1) {
                e1.printStackTrace();
            }
        } finally {
            try {
                ((Connection) connection).close();
            } catch (SQLException e) {
                e.printStackTrace();
            } finally {
                return reimbursements;
            }
        }
    }

    public List<Reimbursement> getAllReimbursementTypes() {
        List<Reimbursement> types = null;
        try {
            //connection = ServiceLocator.getERSDatabase().getConnection();
            types = new ReimbursementDao(connection).queryAll();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                ((Connection) connection).close();
            } catch (SQLException e) {
                e.printStackTrace();
            } finally {
                return types;
            }
        }
    }
}