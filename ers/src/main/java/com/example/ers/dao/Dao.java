package com.example.ers.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import com.example.ers.ResourceHelper;

// this file is a generic DAO.
interface Dao<E> extends ResourceHelper {
    
    //Method for inserting a new record to a table
    void insert(E object) throws SQLException;
    
    // Method for querying all records from a table
    List<E> queryAll() throws SQLException;

    // Method that queries a single record by its id
    E queryById(int id) throws SQLException;

    // Gets the next id for a new record
    int getId() throws SQLException;
    
    // Populates a list with the result set
    void mapRows(ResultSet resultSet, List<E> list) throws SQLException;
}
