package com.example.ers.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.example.ers.model.User;
import com.example.ers.model.UserRole;

public class UserDao implements Dao<User> {
	
	private DBConnection dbc;

    UserDao(DBConnection dbc) {
        this.dbc = dbc;
    }

    public void insert(User object) {
    	try(Connection con = dbc.getDBConnection()){
	        String sql = "INSERT INTO " + userTableName + " VALUES (?,?,?,?,?,?,?)";
	        PreparedStatement preparedStatement = con.prepareStatement(sql);
	        preparedStatement.setInt(1, getId());
	        preparedStatement.setString(2, object.getUsername());
	        preparedStatement.setString(3, object.getPassword());
	        preparedStatement.setString(4, object.getFirstName());
	        preparedStatement.setString(5, object.getLastName());
	        preparedStatement.setString(6, object.getEmail());
	        preparedStatement.setInt(7, object.getRole().getId());
	        preparedStatement.executeUpdate();
    	} catch(SQLException e) {
    		e.printStackTrace();
    	}
    }

    public List<User> queryAll() throws SQLException {
    	try(Connection con = dbc.getDBConnection()){
	        List<User> userList = new ArrayList<User>();
	        String sql = getAllUsers;
	        PreparedStatement preparedStatement = con.prepareStatement(sql);
	        ResultSet resultSet = preparedStatement.executeQuery();
	        mapRows(resultSet, userList);
	        return userList;
    	}
    }

    public User queryById(int id) throws SQLException {
    	try(Connection con = dbc.getDBConnection()){
	        List<User> userList = new ArrayList<User>();
	        String sql = getAllUsers + " WHERE " + userId + "= ?";
	        PreparedStatement preparedStatement = con.prepareStatement(sql);
	        preparedStatement.setInt(1, id);
	        ResultSet resultSet = preparedStatement.executeQuery();
	        mapRows(resultSet, userList);
	        return userList.get(0);
    	}
    }
    
    public User queryByUsername(String username) throws SQLException {
    	try(Connection con = dbc.getDBConnection()){
	        List<User> userList = new ArrayList<User>();
	        String sql = getAllUsers + " WHERE " + userUsername + " = ?";
	        PreparedStatement preparedStatement = con.prepareStatement(sql);
	        preparedStatement.setString(1, username);
	        ResultSet resultSet = preparedStatement.executeQuery();
	        if (resultSet == null) throw new SQLException();
	        mapRows(resultSet, userList);
	        return userList.get(0);
    	}
    }

    public User queryByEmail(String email) throws SQLException {
    	try(Connection con = dbc.getDBConnection()){
	        List<User> userList = new ArrayList<User>();
	        String sql = getAllUsers + " WHERE " + userEmail + " = ?";
	        PreparedStatement preparedStatement = con.prepareStatement(sql);
	        preparedStatement.setString(1, email);
	        ResultSet resultSet = preparedStatement.executeQuery();
	        if (resultSet == null) throw new SQLException();
	        mapRows(resultSet, userList);
	        return userList.get(0);
    	}
    }

    public int getId() throws SQLException {
    	try(Connection con = dbc.getDBConnection()){
	        int id;
	        String sql = "SELECT MAX(" + userId + ") FROM " + userTableName;
	        PreparedStatement preparedStatement = con.prepareStatement(sql);
	        ResultSet resultSet = preparedStatement.executeQuery();
	        if (resultSet.next()) {
	            id = resultSet.getInt(1) + 1;
	            return id;
	        } else throw new SQLException();
    	}
    }

    public void mapRows(ResultSet resultSet, List<User> list) throws SQLException {
    	try(Connection con = dbc.getDBConnection()){
	        while (resultSet.next()) {
	            User user = new User(
	                    resultSet.getInt(userId),
	                    resultSet.getString(userUsername),
	                    resultSet.getString(userPassword),
	                    resultSet.getString(userFirstName),
	                    resultSet.getString(userLastName),
	                    resultSet.getString(userEmail),
	                    new UserRole(
	                            resultSet.getInt(userRoleId),
	                            resultSet.getString(userRole)
	                    )
	            );
	            list.add(user);
	        }
    	}
    }
}
