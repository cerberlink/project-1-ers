package com.example.ers.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DBConnection {
	
	private String url = "jdbc:mariadb://senpai.ceucrzu5qljz.us-west-1.rds.amazonaws.com:3306/ers";
	private String username = "users";
	private String password = "mypassword";
	
	public Connection getDBConnection() throws SQLException{
		return DriverManager.getConnection(url, username, password);
	}
}