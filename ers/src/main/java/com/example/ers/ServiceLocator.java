package com.example.ers;

import java.io.IOException;
import java.io.InputStream;
import java.sql.DriverManager;
import java.util.Properties;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;

import com.example.ers.dao.DBConnection;

public class ServiceLocator {
	
	private static DriverManager ers;
    private static Properties property;
	
    static {
        InputStream stream = ServiceLocator.class.getClassLoader().getResourceAsStream("constants.properties");
        property = new Properties();
        try{
        	property.load(stream);
        }catch (IOException e){
            e.getCause();
            System.out.println("Cannot find the properties file!");
        }
        finally {
            try {
                stream.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    // looks up the ers data source
    private static DriverManager lookupERS(){
        try {
            Context context = new InitialContext(property);
            DriverManager ds = (DriverManager) context.lookup(property.getProperty("ersdb"));
            return ds;
        } catch (NamingException e) {
            e.printStackTrace();
            return null;
        }
    }

    // assigns the data source to the ers class variable
    public synchronized static DriverManager getERSDatabase(){
        if(ers == null) ers = (DriverManager) lookupERS();
        return ers;
    }
}
